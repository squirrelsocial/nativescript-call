import { Observable } from '@nativescript/core';

export default class EventDelegate {
  static _event: Observable = new Observable();
}