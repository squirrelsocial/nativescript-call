import { Observable } from '@nativescript/core';
import EventDelegate from './event-delegate';

import { TNSCall as TNSCallBase, TNSCallReceiveCallOptions } from "./nativescript-call.common";
export class TNSCall implements TNSCallBase {
  receiveCall(options?: TNSCallReceiveCallOptions): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      reject("Not (yet) implemented on Android");
    });
  }

  endCall(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      reject("Not (yet) implemented on Android");
    });
  }

  get event(): Observable {
      return EventDelegate._event;
  }
}